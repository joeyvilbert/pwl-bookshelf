# Bookshelf

> ## Kelompok

| Nama                 | NIM             |
| -------------------- | --------------- |
| Moh Izza Auladina L. | 185150707111009 |
| Joey Vilbert         | 185150707111001 |
| Muhajir              | 185150701111010 |
| Rizkina Arya N.      | 185150701111011 |
| Maysce Christi       | 185150701111009 |

<br>

> ## Screenshoot

![alt text](screenshoot/explore.png)
![alt text](screenshoot/categories.png)
![alt text](screenshoot/authors.png)
![alt text](screenshoot/author-detail.png)
