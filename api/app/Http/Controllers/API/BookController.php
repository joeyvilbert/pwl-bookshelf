<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\ApiHelper;
use App\Models\Book;
use \Exception;

class BookController extends Controller
{
    public function create(Request $request)
    {
        try {
            $fields = $request->validate([
                'author_id' => 'required|integer',
                'category_id' => 'required|integer',
                'name' => 'required|string',
                'year' => 'required|integer',
                'url_image' => 'string',
            ]);

            $book = Book::create($fields);

            return ApiHelper::success($book);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function read($id)
    {
        try {
            $book = Book::findOrFail($id);
            return ApiHelper::success($book);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function readAll()
    {
        $books = Book::all();
        return ApiHelper::success($books);
    }

    public function update(Request $request, $id)
    {
        try {
            $fields = $request->validate([
                'author_id' => 'integer',
                'category_id' => 'integer',
                'name' => 'string',
                'year' => 'integer',
                'url_image' => 'string',
            ]);

            $book = Book::findOrFail($id);
            $book->update($fields);

            return ApiHelper::success($book);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function delete($id)
    {
        try {
            $book = Book::destroy($id);
            return ApiHelper::success($book);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }
}
