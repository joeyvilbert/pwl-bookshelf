<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\ApiHelper;
use App\Models\Category;
use Exception;

class CategoryController extends Controller
{
    public function create(Request $request)
    {
        try {
            $fields = $request->validate([
                'name' => 'required|string',
            ]);

            $category = Category::create($fields);

            return ApiHelper::success($category);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function read($id)
    {
        try {
            $category = Category::findOrFail($id);
            return ApiHelper::success($category);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function readAll()
    {
        $categories = Category::with('books')->get(['id', 'name']);

        return ApiHelper::success($categories);
    }

    public function update(Request $request, $id)
    {
        try {
            $fields = $request->validate([
                'name' => 'required|string',
            ]);

            $category = Category::findOrFail($id);
            $category->update($fields);

            return ApiHelper::success($category);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function delete($id)
    {
        try {
            $category = Category::destroy($id);
            return ApiHelper::success($category);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function readBooks($id)
    {
        try {
            $category = Category::query()
                ->with('books')
                ->findOrFail($id);
            return ApiHelper::success($category);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }
}
