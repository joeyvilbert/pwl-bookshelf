import { createRouter, createWebHistory } from "vue-router";
import Home from "../pages/Home.vue";
import Author from '../pages/Author.vue'
import Category from '../pages/Category.vue'

import DetailAuthor from '../pages/DetailAuthor.vue'

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/category",
    name: "Category",
    component: Category
  },
  {
    path: "/author",
    name: "Author",
    component: Author
  },
  {
    path: "/author/:id",
    name: "AuthorDetail",
    component: DetailAuthor
  },
];

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router